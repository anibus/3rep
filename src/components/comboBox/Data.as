package components.comboBox
{
	import flash.display.Bitmap;
	

	[Bindable]
	public class Data
	{
		public var str:String;
		public var icon:Bitmap;
		public function Data(stra:String,icon:Bitmap)
		{
			str = stra;
			this.icon = icon;
		}
		public function get_str():String{
			return str;
		}
		public function get_bitmap():Bitmap{
			return icon;
		}
	}
}